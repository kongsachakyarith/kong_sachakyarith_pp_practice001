package com.example.dev2pro.controller;

import com.example.dev2pro.repository.EmployeeRepository;
import com.example.dev2pro.springdatajpa.entitiy.Employee;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("getAll")
    public List<Employee> getAll(){
        return employeeRepository.findAll();
    }

    @GetMapping("getById/{id}")
    public Employee getById(@PathVariable Long id){
        return employeeRepository.findById(id);
    }

    @PostMapping("add")
    public Employee addEmp(@RequestParam String name,
                           @RequestParam String gender,
                           @RequestParam String email
    ){
        return employeeRepository.save(new Employee(name,gender,email));
    }

    @DeleteMapping("deleteById/{id}")
    public Employee deleteEmp(@PathVariable Long id){
        return employeeRepository.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Employee updateEmp(@PathVariable long id,
                              @RequestParam String name,
                              @RequestParam String gender,
                              @RequestParam String email){
        return employeeRepository.update(new Employee(id, name,gender,email));
    }

}
